package com.agenda.mappers;

import com.agenda.models.Contato;
import com.agenda.models.dtos.ContatoRequest;

public class ContatoRequestMapper {

    public static Contato fromContatoRequest(ContatoRequest contatoRequest){
        Contato contato = new Contato();
        contato.setTelefone(contatoRequest.getTelefone());
        return contato;
    }
}

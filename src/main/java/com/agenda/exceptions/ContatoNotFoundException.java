package com.agenda.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Contato não encontrado.")
public class ContatoNotFoundException extends RuntimeException {
}

package com.agenda.models.dtos;

public class ContatoRequest {

    String telefone;

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}

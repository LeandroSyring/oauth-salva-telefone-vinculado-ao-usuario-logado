package com.agenda.services;

import com.agenda.exceptions.ContatoNotFoundException;
import com.agenda.models.Contato;
import com.agenda.repositories.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato inserirContato(Contato contato){
        return contatoRepository.save(contato);
    }

    public List<Contato> buscarContatosPorIdUsuario(Integer idUsuario){
        List<Contato> contatoList = contatoRepository.findAllByIdUsuario(idUsuario);

        if(!contatoList.isEmpty()){
            return contatoList;
        }
        throw new ContatoNotFoundException();
    }
}

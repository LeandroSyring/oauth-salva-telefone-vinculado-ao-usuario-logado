package com.agenda.repositories;

import com.agenda.models.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {
    List<Contato> findAllByIdUsuario(Integer idUsuario);
}

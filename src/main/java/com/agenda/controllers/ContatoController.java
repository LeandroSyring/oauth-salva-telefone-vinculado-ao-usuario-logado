package com.agenda.controllers;

import com.agenda.mappers.ContatoRequestMapper;
import com.agenda.models.Contato;
import com.agenda.models.Usuario;
import com.agenda.models.dtos.ContatoRequest;
import com.agenda.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping("/contato")
    public Contato inserirContato(@RequestBody ContatoRequest contatoRequest, @AuthenticationPrincipal Usuario usuario) {
        Contato contato = ContatoRequestMapper.fromContatoRequest(contatoRequest);
        contato.setIdUsuario(usuario.getId());
        Contato contatoSalvo = contatoService.inserirContato(contato);
        return contatoSalvo;
    }

    @GetMapping("/contatos")
    public List<Contato> buscarTodosContatosPorIdUsuario(@AuthenticationPrincipal Usuario usuario) {
        return contatoService.buscarContatosPorIdUsuario(usuario.getId());
    }
}
